﻿namespace Calculatrice
{
    partial class Calculatrice
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtbox_screen = new System.Windows.Forms.TextBox();
            this.btn_1 = new System.Windows.Forms.Button();
            this.btn_2 = new System.Windows.Forms.Button();
            this.btn_3 = new System.Windows.Forms.Button();
            this.btn_4 = new System.Windows.Forms.Button();
            this.btn_5 = new System.Windows.Forms.Button();
            this.btn_6 = new System.Windows.Forms.Button();
            this.btn_9 = new System.Windows.Forms.Button();
            this.btn_8 = new System.Windows.Forms.Button();
            this.btn_7 = new System.Windows.Forms.Button();
            this.btn_reset = new System.Windows.Forms.Button();
            this.btn_equal = new System.Windows.Forms.Button();
            this.btn_0 = new System.Windows.Forms.Button();
            this.btn_virgule = new System.Windows.Forms.Button();
            this.btn_div = new System.Windows.Forms.Button();
            this.btn_multipl = new System.Windows.Forms.Button();
            this.btn_min = new System.Windows.Forms.Button();
            this.btn_plus = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtbox_screen
            // 
            this.txtbox_screen.Enabled = false;
            this.txtbox_screen.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtbox_screen.Location = new System.Drawing.Point(12, 12);
            this.txtbox_screen.Multiline = true;
            this.txtbox_screen.Name = "txtbox_screen";
            this.txtbox_screen.Size = new System.Drawing.Size(357, 140);
            this.txtbox_screen.TabIndex = 0;
            // 
            // btn_1
            // 
            this.btn_1.Location = new System.Drawing.Point(12, 183);
            this.btn_1.Name = "btn_1";
            this.btn_1.Size = new System.Drawing.Size(65, 50);
            this.btn_1.TabIndex = 1;
            this.btn_1.Text = "1";
            this.btn_1.UseVisualStyleBackColor = true;
            // 
            // btn_2
            // 
            this.btn_2.Location = new System.Drawing.Point(102, 183);
            this.btn_2.Name = "btn_2";
            this.btn_2.Size = new System.Drawing.Size(65, 50);
            this.btn_2.TabIndex = 2;
            this.btn_2.Text = "2";
            this.btn_2.UseVisualStyleBackColor = true;
            // 
            // btn_3
            // 
            this.btn_3.Location = new System.Drawing.Point(193, 183);
            this.btn_3.Name = "btn_3";
            this.btn_3.Size = new System.Drawing.Size(65, 50);
            this.btn_3.TabIndex = 3;
            this.btn_3.Text = "3";
            this.btn_3.UseVisualStyleBackColor = true;
            // 
            // btn_4
            // 
            this.btn_4.Location = new System.Drawing.Point(12, 257);
            this.btn_4.Name = "btn_4";
            this.btn_4.Size = new System.Drawing.Size(65, 50);
            this.btn_4.TabIndex = 4;
            this.btn_4.Text = "4";
            this.btn_4.UseVisualStyleBackColor = true;
            // 
            // btn_5
            // 
            this.btn_5.Location = new System.Drawing.Point(102, 257);
            this.btn_5.Name = "btn_5";
            this.btn_5.Size = new System.Drawing.Size(65, 50);
            this.btn_5.TabIndex = 5;
            this.btn_5.Text = "5";
            this.btn_5.UseVisualStyleBackColor = true;
            // 
            // btn_6
            // 
            this.btn_6.Location = new System.Drawing.Point(193, 257);
            this.btn_6.Name = "btn_6";
            this.btn_6.Size = new System.Drawing.Size(65, 50);
            this.btn_6.TabIndex = 6;
            this.btn_6.Text = "6";
            this.btn_6.UseVisualStyleBackColor = true;
            // 
            // btn_9
            // 
            this.btn_9.Location = new System.Drawing.Point(193, 331);
            this.btn_9.Name = "btn_9";
            this.btn_9.Size = new System.Drawing.Size(65, 50);
            this.btn_9.TabIndex = 9;
            this.btn_9.Text = "9";
            this.btn_9.UseVisualStyleBackColor = true;
            // 
            // btn_8
            // 
            this.btn_8.Location = new System.Drawing.Point(102, 331);
            this.btn_8.Name = "btn_8";
            this.btn_8.Size = new System.Drawing.Size(65, 50);
            this.btn_8.TabIndex = 8;
            this.btn_8.Text = "8";
            this.btn_8.UseVisualStyleBackColor = true;
            // 
            // btn_7
            // 
            this.btn_7.Location = new System.Drawing.Point(12, 331);
            this.btn_7.Name = "btn_7";
            this.btn_7.Size = new System.Drawing.Size(65, 50);
            this.btn_7.TabIndex = 7;
            this.btn_7.Text = "7";
            this.btn_7.UseVisualStyleBackColor = true;
            // 
            // btn_reset
            // 
            this.btn_reset.ForeColor = System.Drawing.Color.Red;
            this.btn_reset.Location = new System.Drawing.Point(284, 183);
            this.btn_reset.Name = "btn_reset";
            this.btn_reset.Size = new System.Drawing.Size(75, 50);
            this.btn_reset.TabIndex = 10;
            this.btn_reset.Text = "C";
            this.btn_reset.UseVisualStyleBackColor = true;
            // 
            // btn_equal
            // 
            this.btn_equal.Location = new System.Drawing.Point(193, 406);
            this.btn_equal.Name = "btn_equal";
            this.btn_equal.Size = new System.Drawing.Size(65, 50);
            this.btn_equal.TabIndex = 17;
            this.btn_equal.Text = "=";
            this.btn_equal.UseVisualStyleBackColor = true;
            // 
            // btn_0
            // 
            this.btn_0.Location = new System.Drawing.Point(102, 406);
            this.btn_0.Name = "btn_0";
            this.btn_0.Size = new System.Drawing.Size(65, 50);
            this.btn_0.TabIndex = 16;
            this.btn_0.Text = "0";
            this.btn_0.UseVisualStyleBackColor = true;
            // 
            // btn_virgule
            // 
            this.btn_virgule.Location = new System.Drawing.Point(12, 406);
            this.btn_virgule.Name = "btn_virgule";
            this.btn_virgule.Size = new System.Drawing.Size(65, 50);
            this.btn_virgule.TabIndex = 15;
            this.btn_virgule.Text = ".";
            this.btn_virgule.UseVisualStyleBackColor = true;
            // 
            // btn_div
            // 
            this.btn_div.Location = new System.Drawing.Point(284, 416);
            this.btn_div.Name = "btn_div";
            this.btn_div.Size = new System.Drawing.Size(75, 40);
            this.btn_div.TabIndex = 21;
            this.btn_div.Text = "/";
            this.btn_div.UseVisualStyleBackColor = true;
            // 
            // btn_multipl
            // 
            this.btn_multipl.Location = new System.Drawing.Point(284, 361);
            this.btn_multipl.Name = "btn_multipl";
            this.btn_multipl.Size = new System.Drawing.Size(75, 40);
            this.btn_multipl.TabIndex = 22;
            this.btn_multipl.Text = "X";
            this.btn_multipl.UseVisualStyleBackColor = true;
            // 
            // btn_min
            // 
            this.btn_min.Location = new System.Drawing.Point(284, 303);
            this.btn_min.Name = "btn_min";
            this.btn_min.Size = new System.Drawing.Size(75, 40);
            this.btn_min.TabIndex = 23;
            this.btn_min.Text = "-";
            this.btn_min.UseVisualStyleBackColor = true;
            // 
            // btn_plus
            // 
            this.btn_plus.Location = new System.Drawing.Point(284, 247);
            this.btn_plus.Name = "btn_plus";
            this.btn_plus.Size = new System.Drawing.Size(75, 40);
            this.btn_plus.TabIndex = 24;
            this.btn_plus.Text = "+";
            this.btn_plus.UseVisualStyleBackColor = true;
            // 
            // Calculatrice
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(381, 492);
            this.Controls.Add(this.btn_plus);
            this.Controls.Add(this.btn_min);
            this.Controls.Add(this.btn_multipl);
            this.Controls.Add(this.btn_div);
            this.Controls.Add(this.btn_equal);
            this.Controls.Add(this.btn_0);
            this.Controls.Add(this.btn_virgule);
            this.Controls.Add(this.btn_reset);
            this.Controls.Add(this.btn_9);
            this.Controls.Add(this.btn_8);
            this.Controls.Add(this.btn_7);
            this.Controls.Add(this.btn_6);
            this.Controls.Add(this.btn_5);
            this.Controls.Add(this.btn_4);
            this.Controls.Add(this.btn_3);
            this.Controls.Add(this.btn_2);
            this.Controls.Add(this.btn_1);
            this.Controls.Add(this.txtbox_screen);
            this.Name = "Calculatrice";
            this.Text = "Calculatrice";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtbox_screen;
        private System.Windows.Forms.Button btn_1;
        private System.Windows.Forms.Button btn_2;
        private System.Windows.Forms.Button btn_3;
        private System.Windows.Forms.Button btn_4;
        private System.Windows.Forms.Button btn_5;
        private System.Windows.Forms.Button btn_6;
        private System.Windows.Forms.Button btn_9;
        private System.Windows.Forms.Button btn_8;
        private System.Windows.Forms.Button btn_7;
        private System.Windows.Forms.Button btn_reset;
        private System.Windows.Forms.Button btn_equal;
        private System.Windows.Forms.Button btn_0;
        private System.Windows.Forms.Button btn_virgule;
        private System.Windows.Forms.Button btn_div;
        private System.Windows.Forms.Button btn_multipl;
        private System.Windows.Forms.Button btn_min;
        private System.Windows.Forms.Button btn_plus;
    }
}

